import React, { createContext, useContext, useState } from "react";
import {
    createActivityRepository,
    getActivitiesRepository,
    deleteActivityRepository,
    editActivityRepository
} from "@/repositories/activityRepository";

const ActivitesContext = createContext();

export function ActivitesContextFunctions() {
    const [activities, setActivities] = useState([]);

    async function addActivity(data, ID) {
        try {
            await createActivityRepository(data, ID);
            getActivities();
        } catch (error) {
            throw error;
        }
    }

    async function getActivities() {
        try {
            const res = await getActivitiesRepository();
            setActivities(res.data?.data.activities);
            return res;
        } catch (error) {
            throw error;
        }
    }

    async function editActivity({data, activityTypeID, activityID}) {
        try {
            const res = await editActivityRepository(data, activityTypeID, activityID);
            getActivities();
            return res;
        } catch (error) {
            throw error;
        }
    }

    async function deleteActivity(activityTypeID, activityID) {
        try {
            const res = await deleteActivityRepository(activityTypeID, activityID);
            getActivities();
            return res;
        } catch (error) {
            throw error;
        }
    }

    return {
        addActivity,
        getActivities,
        editActivity,
        deleteActivity,
        activities,
        setActivities
    };
}

export function ActivitesContextProvider({ children }) {
    const contextValue = ActivitesContextFunctions();

    return (
        <ActivitesContext.Provider value={contextValue}>
            {children}
        </ActivitesContext.Provider>
    );
}

export function useActivitesContext() {
    return useContext(ActivitesContext);
}
