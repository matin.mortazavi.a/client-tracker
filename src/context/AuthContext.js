import React, { createContext, useContext, useState } from "react";
import {
    loginRepository,
    registerRepository
} from "@/repositories/authenticationRepository";

const AuthContext = createContext();
export function AuthContextFunctions() {
    const [user, setUser] = useState(null);
    const [prePareUser, setPrePareUser] = useState(null);

    const loginUser = async (data) => {
        try {
            await loginRepository(data);
        } catch (error) {
            throw error;
        }
    };

    const signoutUser = () => {};

    const registerUser = async (data) => {
        data.age = Number(data.age);
         const newUser = { ...prePareUser, ...data, age: Number(data.age) };
        ;
        try {
            await registerRepository(newUser);
        } catch (error) {
            throw error;
        }
    };
    return {
        user,
        prePareUser,
        setPrePareUser,
        setUser,
        loginUser,
        signoutUser,
        registerUser
    };
}
export function AuthProvider({ children }) {
    const contextValue = AuthContextFunctions();
    return (
        <AuthContext.Provider
            value={contextValue}
        >
            {children}
        </AuthContext.Provider>
    );
}

export function useAuthContext() {
    return useContext(AuthContext);
}
