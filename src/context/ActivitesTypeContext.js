import React, { createContext, useContext, useState } from "react";
import {
    createActivityTypeRepository,
    getActivitiyTypesListRepository,
    deleteActivityTypeRepository,
    editActivityTypeRepository
} from "@/repositories/activityTypeRepository";

const ActivitesTypeContext = createContext();

export function ActivitesTypeContextFunctions() {
    const [activityTypesList, setActivityTypesList] = useState([]);

    async function addActivityType(data) {
        try {
            await createActivityTypeRepository(data);
            getActivityTypesList();
        } catch (error) {
            throw error;
        }
    }

    async function getActivityTypesList() {
        try {
            const res = await getActivitiyTypesListRepository();
            setActivityTypesList(res.data?.data.activityTypes);
            return res;
        } catch (error) {
            throw error;
        }
    }

    async function editActivityType(id, data) {
        try {
            const res = await editActivityTypeRepository(id, data);
            getActivityTypesList();
            return res;
        } catch (error) {
            throw error;
        }
    }

    async function deleteActivityType(id) {
        try {
            const res = await deleteActivityTypeRepository(id);
            getActivityTypesList();
            return res;
        } catch (error) {
            throw error;
        }
    }

    return {
        deleteActivityType,
        editActivityType,
        addActivityType,
        getActivityTypesList,
        activityTypesList
    };
}

export function ActivitesTypeContextProvider({ children }) {
    const contextValue = ActivitesTypeContextFunctions();

   
    return (
        <ActivitesTypeContext.Provider value={contextValue}>
            {children}
        </ActivitesTypeContext.Provider>
    );
}

export function useActivitesTypeContext() {
    return useContext(ActivitesTypeContext);
}
