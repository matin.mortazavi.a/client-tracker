import React, { createContext, useContext, useState } from "react";
import profileRepository from "@/repositories/profileRepository";

const UserContext = createContext();

export function UserContextFunctions() {
    const [user, setUser] = useState(null);

    const getUserInfo = async () => {
        try {
            const userData = await profileRepository();
            setUser(userData.data.data.user);
        } catch (error) {
            return error;
        }
    };

    return {
        user,
        getUserInfo
    };
}

export const UserProvider = ({ children }) => {
    const contextValue = UserContextFunctions();

    return (
        <UserContext.Provider value={contextValue}>
            {children}
        </UserContext.Provider>
    );
};

export const useUserContext = () => {
    return useContext(UserContext);
};
