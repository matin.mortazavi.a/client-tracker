import React, { createContext, useContext, useState } from "react";
import {
    createEventRepository,
    getEventsRepository,
    deleteEventRepository,
    editEventRepository
} from "@/repositories/eventRepository";

const EventsContext = createContext();

export function EventsContextFunctions() {
    const [events, setEvents] = useState([]);

    async function addEvent(data) {
        try {
            await createEventRepository(data);
            getEvents();
        } catch (error) {
            throw error;
        }
    }

    async function getEvents() {
        try {
            const res = await getEventsRepository();
            setEvents(res.data?.data.events);
            return res;
        } catch (error) {
            throw error;
        }
    }

    async function editEvent(data,eventID) {
        try {
            const res = await editEventRepository(data,eventID);
            getEvents();
            return res;
        } catch (error) {
            throw error;
        }
    }

    async function deleteEvent(eventID) {
        try {
            const res = await deleteEventRepository(eventID);
            getEvents();
            return res;
        } catch (error) {
            throw error;
        }
    }

    return {
        addEvent,
        getEvents,
        editEvent,
        deleteEvent,
        events,
        setEvents
    };
}

export function EventsContextProvider({ children }) {
    const contextValue = EventsContextFunctions();

    return (
        <EventsContext.Provider value={contextValue}>
            {children}
        </EventsContext.Provider>
    );
}

export function useEventsContext() {
    return useContext(EventsContext);
}
