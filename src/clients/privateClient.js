import axios from "axios";
import privateInterceptor from "@/interceptors/privateInterceptor";

const privateClient = axios.create({
    baseURL: "https://frontend-internship-api.atadp.com"
});
privateInterceptor(privateClient)
export default privateClient;

