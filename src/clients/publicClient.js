import axios from "axios";
import publicInterceptor from "@/interceptors/publicInterceptor";
const publicClient = axios.create({
    baseURL: "https://frontend-internship-api.atadp.com"
});
publicInterceptor(publicClient)
export default publicClient

