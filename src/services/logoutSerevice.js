import localStorageService from "@/services/localStorageService";

export default function logoutSerivce(){
    localStorageService.remove("userToken");
    window.location.href = "/login";
}