var moment = require("jalali-moment")
const getPersianDateService = (dateString) => {
    const persianMonths = [
        'فروردین',
        'اردیبهشت',
        'خرداد',
        'تیر',
        'مرداد',
        'شهریور',
        'مهر',
        'آبان',
        'آذر',
        'دی',
        'بهمن',
        'اسفند',
      ];
  const miladiDate = new Date(dateString).toLocaleDateString('en-US');
  const splitedDate = miladiDate.split('/');
  const year = parseInt(splitedDate[2], 10);
  const month = parseInt(splitedDate[0], 10);
  const day = parseInt(splitedDate[1], 10);
  const persianDate = moment(`${year}/${month}/${day}`, 'YYYY/MM/DD').locale('fa').format('YYYY/MM/DD');
  const persianDateSplited = persianDate.split('/');
  const persianMonthIndex = parseInt(persianDateSplited[1], 10);
  const jalaliDay = parseInt(persianDateSplited[2], 10);
  const persianMonth = persianMonths[persianMonthIndex - 1];
  const hour = new Date(dateString).getHours();
  const minutes = new Date(dateString).getMinutes();

  return {
    persianMonthIndex,
    jalaliDay,
    persianMonth,
    hour,
    minutes,
  };
};

export default getPersianDateService;
