import jalaliMoment from "jalali-moment";

const now = new Date();
var moment = require("jalali-moment");
const getCurrentJalaliWeekNumber = () => {
    const jalaliNow = moment().local("fa");
    return jalaliNow.format("w");
};

const filterItemByDateService = (items, itemDate, filterType) => {
    if (filterType === "year") {
        const jalaliYear = jalaliMoment(now).format("jYYYY");
        return items.filter(
            (item) =>
                jalaliMoment(new Date(item[itemDate])).format("jYYYY") ==
                jalaliYear
        );
    } else if (filterType == "month") {
        const jalaliMonth = jalaliMoment(now).format("jMM");
        return items.filter(
            (item) =>
                jalaliMoment(new Date(item[itemDate])).format("jMM") ==
                jalaliMonth
        );
    } else if (filterType === "week") {
        const currentWeekNumber = getCurrentJalaliWeekNumber();
        return items.filter(
            (item) =>
                jalaliMoment(new Date(item[itemDate])).format("w") ==
                currentWeekNumber
        );
    } else if (filterType === "day") {
        const jalaliDay = jalaliMoment(now).format("jDD");

        return items.filter(
            (item) =>
                jalaliMoment(new Date(item[itemDate])).format("jDD") ==
                jalaliDay
        );
    } else {
        return items;
    }
};

export default filterItemByDateService;
