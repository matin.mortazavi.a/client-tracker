import React from "react";
import { useForm } from "react-hook-form";
import styles from "./Form.module.scss"; // Import your CSS module
import { useState } from "react";
import CustomInput from "@components/formFields/customInput/CustomInput";

import { useNavigate } from "react-router-dom";
import { useAuthContext } from "@/context/AuthContext";
import CustomButton from "@components/Common/customButton/CustomButton";

function RegisterPage() {
    const { setPrePareUser } = useAuthContext();
    const [passwordVisible, setPasswordVisible] = useState(false);
    const {
        register,
        handleSubmit,
        watch,
        reset,
        formState: { errors }
    } = useForm({
        mode: "onBlur"
    });

    const handlePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
    };

    const navigate = useNavigate();

    const onSubmit = async (data) => {
        reset();
        setPrePareUser(data);
        navigate("/complite-data/");
    };
    const inputsDetails = [
        {
            id: 1,
            name: "email",
            label: "ایمیل",
            iconName: "userIcon",
            type: "email",
            errors: errors["email"],
            value: watch("email"),
            rules: { required: "این فیلد الزامی است" }
        },
        {
            id: 2,
            name: "password",
            label: "رمز عبور",
            passIconName: "eye",
            iconName: "lock",
            type: passwordVisible ? "text" : "password",
            errors: errors["password"],
            value: watch("password"),
            rules: { required: "این فیلد الزامی است" },
            handlePasswordVisibility: handlePasswordVisibility
        }
    ];

    return (
        <form
            className={styles["form-content"]}
            onSubmit={handleSubmit(onSubmit)}
        >
            {inputsDetails.map((input) => (
                <CustomInput
                    inputdetails={{ ...register(input.name, input.rules) }}
                    key={input.id}
                    {...input}
                />
            ))}

            <CustomButton text=" ثبت نام"></CustomButton>
            <span className={styles["form__text"]}></span>
        </form>
    );
}

export default RegisterPage;
