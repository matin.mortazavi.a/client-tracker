import React from "react";
import { useForm } from "react-hook-form";
import styles from "./Form.module.scss";
import { useState } from "react";
import { useAuthContext } from "@/context/AuthContext";
import { Alert } from "@components/Common";
import CustomInput from "@components/formFields/customInput/CustomInput";
import CustomButton from "@components/Common/customButton/CustomButton";
import { useNavigate } from "react-router-dom";

function CompliteData() {
    const navigate = useNavigate()
    const { registerUser } = useAuthContext();
    const [showAlert, setShowAlert] = useState(false);
    const {
        register,
        handleSubmit,
        watch,
        reset,
        formState: { errors }
    } = useForm({
        mode: "onBlur"
    });

    const onSubmit = async (data) => {
        reset();
        try {
            await registerUser(data);
            navigate("/")
        } catch {
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
            }, 2000);
        }
    };

    const inputsDetails = [
        {
            id: 1,
            name: "name",
            label: "نام و نام خانوادگی",
            iconName: "userIcon",
            type: "text",
            errors: errors["name"],
            isEmpty: watch("name"),
            rules: { required: "این فیلد الزامی است" }
        },
        {
            id: 2,
            name: "age",
            label: "سن",
            iconName: "calendar",
            type: "number",
            errors: errors["age"],
            isEmpty: watch("age"),
            rules: { required: "این فیلد الزامی است" }
        }
    ];

    return (
        <form
            className={styles["form-content"]}
            onSubmit={handleSubmit(onSubmit)}
        >
            {showAlert && (
                <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
            )}
            {inputsDetails.map((input) => (
                <CustomInput
                    inputdetails={{ ...register(input.name, input.rules) }}
                    key={input.id}
                    {...input}
                />
            ))}

            <CustomButton text="تایید"></CustomButton>
            <span className={styles["form__text"]}></span>
        </form>
    );
}

export default CompliteData;
