import React from "react";
import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import styles from "./Form.module.scss";
import { useState } from "react";
import { Alert } from "@components/Common";
import CustomInput from "@components/formFields/customInput/CustomInput";
import { useAuthContext } from "@/context/AuthContext";
import CustomButton from "@components/Common/customButton/CustomButton";

function Login() {
    const navigate = useNavigate()
    const { loginUser } = useAuthContext();
    const [passwordVisible, setPasswordVisible] = useState(false);

    const [showAlert, setShowAlert] = useState(false);

    const {
        register,
        handleSubmit,
        watch,
        reset,
        formState: { errors }
    } = useForm({
        mode: "onBlur"
    });

    const handlePasswordVisibility = () => {
        setPasswordVisible(!passwordVisible);
    };

    const onSubmit = async (data) => {
        reset();
        try {  
            await loginUser(data);
            navigate("/")
            
        } catch (error) {
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
            }, 2000);
        }
    };

    const inputsDetails = [
        {
            id: 1,
            name: "email",
            label: "ایمیل",
            iconName: "userIcon",
            type: "email",
            errors: errors["email"],
            value: watch("email"),
            rules: { required: "این فیلد الزامی است" },
            passVisibilty: handlePasswordVisibility
        },
        {
            id: 2,
            name: "password",
            label: "رمز عبور",
            passIconName: "eye",
            iconName: "lock",
            type: passwordVisible ? "text" : "password",
            errors: errors["password"],
            value: watch("password"),
            rules: { required: "این فیلد الزامی است" },
            handlePasswordVisibility: handlePasswordVisibility
        }
    ];

    return (
        <form
            className={styles["form-content"]}
            onSubmit={handleSubmit(onSubmit)}
        >
            {showAlert && (
                <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
            )}
            {inputsDetails.map((input) => (
                <CustomInput
                    inputdetails={{ ...register(input.name, input.rules) }}
                    key={input.id}
                    {...input}
                />
            ))}

            <CustomButton text="ورود"></CustomButton>
            <span className={styles["form__text"]}>
                حساب کاربری ندارید؟{" "}
                <Link className={styles["form__link"]} to={"/register"}>
                    ثبت نام
                </Link>
            </span>
        </form>
    );
}

export default Login;
