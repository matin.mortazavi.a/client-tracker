import React from "react";
import styles from "./Dashboard.module.scss";
import Activities from "@components/activities/Activities";
import ActivityTypes from "@components/activityTypes/ActivityTypes";
import Events from "@components/events/Events";
import {
    Reports,

} from "@components/dashboard";

export default function Dashboard() {

    return (
        <div className={styles.container}>
            <Reports></Reports>
            <Events></Events>
            <Activities></Activities>
            <ActivityTypes></ActivityTypes> 
        </div>
    );
}
