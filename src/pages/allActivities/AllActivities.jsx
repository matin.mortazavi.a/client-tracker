import React, { useEffect } from 'react';
import { useActivitesContext } from '@/context/ActivitesContext';
import {ActivityItem,Alert} from '@components/Common';
import styles from './AllActivities.module.scss';
import { useState } from 'react';
import filterItemByDateService from '@/services/filterItemByDateService';

export default function AllActivities() {
  const { getActivities, activities } = useActivitesContext();
  const [filteredActivities, setFilteredActivities] = useState([]);
  const [filterType, setFilterType] = useState(null);
  const [showAlert, setShowAlert] = useState(false);
 
  useEffect(() => {
    try{
      getActivities();
    }catch (error) {
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
            }, 2000);
        }
  }, [getActivities]);

  useEffect(() => {
    setFilteredActivities(filterItemByDateService(activities,'date',filterType))
  }, [filterType, activities]);



  return (
    <>
     {showAlert && (
                <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
      )}
      <div className={styles['activity-details']}>
        <p className={styles['activity-title']}>فعالیت ها</p>
        <div className={styles['activity-filtering']}>
          <span
            className={styles['activity-filtering__item']}
            href=""
            onClick={() => setFilterType('day')}
          >
            روزانه
          </span>
          <span
            className={styles['activity-filtering__item']}
            onClick={() => setFilterType('week')}
           
          >
            هفتگی
          </span>
          <span
                 className={styles['activity-filtering__item']}
                 onClick={() =>setFilterType('month')}
          >
            ماهانه
          </span>
          <span
                 className={styles['activity-filtering__item']}
                  onClick={()=> setFilterType('year')}
          >
            سالانه
          </span>
        </div>
      </div>

      <div className={styles['activities']}>
        {filteredActivities.map((activity) => (
          <ActivityItem key={activity.id} {...activity} />
        ))}
      </div>
    </>
  );
}
