import React, { useEffect } from 'react';
import {useEventsContext} from "@/context/EventsContext"
import {EventItem,Alert} from "@components/Common"
import styles from './AllEvents.module.scss';
import { useState } from 'react';
import filterItemByDateService from '@/services/filterItemByDateService';
export default function AllEvents() {
  const { getEvents, events } = useEventsContext()
  const [filteredEvent, setFilteredEvent] = useState([]);
  const [filterType, setFilterType] = useState(null);
  const [showAlert, setShowAlert] = useState(false);

 
  useEffect(() => {
    try{
      getEvents();
    }catch (error) {
      setShowAlert(true);
      setTimeout(() => {
          setShowAlert(false);
      }, 2000);
  }
    
  }, [getEvents]);
  useEffect(() => {
    setFilteredEvent(filterItemByDateService(events,'date',filterType))
  }, [filterType, events]);



  return (
    <>
     {showAlert && (
                <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
      )}
      <div className={styles['event-details']}>
        <p className={styles['event-title']}>فعالیت ها</p>
        <div className={styles['event-filtering']}>
          <span
            className={styles['event-filtering__item']}
            href=""
            onClick={() => setFilterType('day')}
          >
            روزانه
          </span>
          <span
            className={styles['event-filtering__item']}
            onClick={() => setFilterType('week')}
           
          >
            هفتگی
          </span>
          <span
                 className={styles['event-filtering__item']}
                 onClick={() =>setFilterType('month')}
          >
            ماهانه
          </span>
          <span
                 className={styles['event-filtering__item']}
                  onClick={()=> setFilterType('year')}
          >
            سالانه
          </span>
        </div>
      </div>

      <div className={styles['events']}>
        {filteredEvent.map((event) => (
          <EventItem key={event.id} {...event} />
        ))}
      </div>
    </>
  );
}
