import "./App.scss";

import { useRoutes } from "react-router-dom";
import appRouter from "./routes/appRouter";
import { AuthProvider } from "./context/AuthContext";
import { UserProvider } from "./context/UserContext";
import { ActivitesTypeContextProvider } from "@/context/ActivitesTypeContext";
import { ActivitesContextProvider } from "@/context/ActivitesContext";
import { EventsContextProvider } from "@/context/EventsContext";
function App() {
  const router = useRoutes(appRouter);
  return (
    <>
      <AuthProvider>
        <ActivitesContextProvider>
          <ActivitesTypeContextProvider>
            <EventsContextProvider>
              <UserProvider>{router}</UserProvider>
            </EventsContextProvider>
          </ActivitesTypeContextProvider>
        </ActivitesContextProvider>
      </AuthProvider>
    </>
  );
}

export default App;
