
import Register from "@/pages/auth/Register";
import Dashboard from "@/pages/dashboard/Dashboard";
import CompliteData from "@/pages/auth/CompleteData";
import AuthLayout from "@/components/layouts/authLayout/AuthLayout";
import Login from "@/pages/auth/Login";
import PanelLayout from "@components/layouts/panelLayout/PanelLayout";
import ProtectedRoute from "@components/ProtectedRoute";
import { Navigate } from "react-router-dom";
import AllActivities from "@/pages/allActivities/AllActivities";
import AllEvents from "@/pages/allEvents/AllEvents"
const routes = [
    {
        path: "/",
        element: <Navigate to="/dashboard" />
    },
    {
        element: <ProtectedRoute />,
        children: [
            {
                path: "/dashboard",
                element: (
                    <PanelLayout>
                        <Dashboard />
                    </PanelLayout>
                )
            },
            {
                path : "/activities",
                element : (
                    <PanelLayout>
                        <AllActivities />
                    </PanelLayout>
                )
            },
            {
                path : "/events",
                element : (
                    <PanelLayout>
                        <AllEvents />
                    </PanelLayout>
                )
            }
            
        ]
    },

    {
        path: "/login",
        element: (
            <AuthLayout name="ورود ">
                <Login />
            </AuthLayout>
        )
    },
    {
        path: "/complite-data",
        element: (
            <AuthLayout name="اطلاعات فردی ">
                <CompliteData />
            </AuthLayout>
        )
    },
    {
        path: "/register",
        element: (
            <AuthLayout name="ثبت نام">
                <Register />
            </AuthLayout>
        )
    }
];

export default routes;
