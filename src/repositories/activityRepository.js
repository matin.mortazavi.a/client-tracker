import privateClient from "@/clients/privateClient";

async function createActivityRepository(data, activityTypeID) {
    const endpoint = `/api/v1/activities/${activityTypeID}`;
    const response = await privateClient.post(endpoint, data);
    return response;

}
async function getActivitiesRepository() {

    const endpoint = `/api/v1/activities/getAllActivityChilds`;
    const response =  privateClient.get(endpoint);
    return response;
}

async function deleteActivityRepository(activtyTypeID, activityID) {
    const endpoint = `/api/v1/activities/${activtyTypeID}/${activityID}`;
    const response = await privateClient.delete(endpoint);
    return response;
}
async function editActivityRepository(data, activtyTypeID, activityID) {
  
    const endpoint = `/api/v1/activities/${activtyTypeID}/${activityID}`;
    const response = await privateClient.put(endpoint, data);
    return response;
}

export {
    createActivityRepository,
    getActivitiesRepository,
    deleteActivityRepository,
    editActivityRepository
};
