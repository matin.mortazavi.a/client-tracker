import publicClient from "../clients/publicClient";
async function registerRepository(data) {
    const endpoint = "/api/v1/auth/signUp";
    const response = await publicClient.post(endpoint, data);
    return response;
}

async function loginRepository(data) {
    const endpoint = "/api/v1/auth/login";
    const response = await publicClient.post(endpoint, data);
    return response;
}

export { registerRepository, loginRepository };
