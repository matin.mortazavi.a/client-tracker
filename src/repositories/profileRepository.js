import privateClient from "@/clients/privateClient";
async function profileRepository() {
    const endpoint = "/api/v1/users/profile";
    const response = await privateClient.get(endpoint);
    return response;
}

export default profileRepository;
