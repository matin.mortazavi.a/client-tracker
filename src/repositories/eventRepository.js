import privateClient from "@/clients/privateClient";

async function createEventRepository(data) {
  if(navigator.onLine){
    const endpoint = `/api/v1/events`;
    const response = await privateClient.post(endpoint, data);
    return response;

  }
   
}
async function getEventsRepository() {
    if(navigator.onLine){
        const endpoint = `/api/v1/events`;
        const response =  privateClient.get(endpoint);
        return response;
    }
   
}

async function deleteEventRepository(eventID) {
  if(navigator.onLine){
    const endpoint = `/api/v1/events/${eventID}`;
    const response = await privateClient.delete(endpoint);
    return response;
  }
}
async function editEventRepository(data,eventID) {
  if(navigator.onLine){
    const endpoint = `/api/v1/events/${eventID}`;
    const response = await privateClient.put(endpoint, data);
    return response;
  }
}

export {
    createEventRepository,
    getEventsRepository,
    deleteEventRepository,
    editEventRepository
};
