import privateClient from "@/clients/privateClient";
async function createActivityTypeRepository(data) {
    const endpoint = "/api/v1/activities";
    const response = await privateClient.post(endpoint, data);
    return response;
}
async function getActivitiyTypesListRepository() {
    
    const endpoint = "/api/v1/activities";
    const response = await privateClient.get(endpoint);
    return response;
}

async function deleteActivityTypeRepository(id) {
    const endpoint = `/api/v1/activities/${id}`;
    const response = await privateClient.delete(endpoint);
    return response;
}
async function editActivityTypeRepository(id, data) {
    const endpoint = `/api/v1/activities/${id}`;
    const response = await privateClient.put(endpoint, data);
    return response;
}

export {
    createActivityTypeRepository,
    getActivitiyTypesListRepository,
    deleteActivityTypeRepository,
    editActivityTypeRepository
};
