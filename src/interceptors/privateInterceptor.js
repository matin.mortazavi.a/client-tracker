import localStorageService from "@/services/localStorageService";
import logoutSerivce from "@/services/logoutSerevice";
export default function privateInterceptor(client){
    client.interceptors.request.use(
        (config) => {
            const authToken = localStorageService.get("userToken");
    
            if (authToken) {
                config.headers.Authorization = `Bearer ${authToken}`;
            }
    
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );
    
    client.interceptors.response.use(
        (response) => {
            return response;
        },
        (error) => {
            if (error.response && error.response.status === 401) {
                logoutSerivce()
                throw new Error("Unauthorized");
            }
    
            return Promise.reject(error);
        }
    );
    
}
