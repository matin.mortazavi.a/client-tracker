import axios from "axios";
import localStorageService from "@/services/localStorageService";

import publicClient from "@/clients/publicClient";
function publicInterceptor(client){
    client.interceptors.response.use(
        (response) => {
            if (response.status === 200) {
                localStorageService.set("userToken", response.data.data.token);
                return response.data;
            } else {
                throw new Error("bad request");
            }
        },
        (error) => {
            return Promise.reject(error);
        }
    );
}

export default publicInterceptor;
