import React from 'react'
import styles from "./CustomImageUploader.module.scss"
import { useRef } from 'react'
import SvgLoader from "@components/SvgLoader"
export default function CustomImageUploader(props) {
 const image = props.image
    const fileInputRef = useRef(null);
  return (
    !props.image  ? (
        <div  onClick={() => fileInputRef.current.click()} className={styles['photo-uploader']}>
    <input type="file" className={styles['photo-uploader__input']}
    onChange={(e)=>props.onChange(e.target.files[0])}
    ref={fileInputRef}/>
    
    <SvgLoader name={props.iconName} className={styles[ "photo-uploader__icon"]}/>
    
    </div>
    )  : (
        <div onClick={() => fileInputRef.current.click()} className={styles['photo-uploader']}>
           <input type="file" className={styles['photo-uploader__input']}
    onChange={(e)=>props.onChange(e.target.files[0])}
    ref={fileInputRef}/>
     <img className={styles['photo-uploader__image']} src={image} alt="" />
           

        </div>
    )
     
    
  )
}
