// CustomDatePicker.js
import React, { useRef } from 'react';
import TimePicker from '@ashwinthomas/react-time-picker-dropdown';

import FormFieldLayout from '@components/layouts/formFeildsLayout/FormFieldLayout';

import "./custom.scss";

const CustomTimePicker = ({time, name, label, iconName, passIconName, onChange = ()=>{},defaultValue , ...props }) => {
const ref = document.querySelectorAll(".tp_confirmBtn")
ref.forEach((r)=>{
  if(r){
    r.setAttribute("type", "button");
    
  }
})



  const onTimeChange = (data)=>{
    data = data.replace(/\s/g, '')
   data =  data.replace(/am|pm/gi, '');
   data = data.slice(0, -3)
   onChange(data)
  }

  return (
    <FormFieldLayout isNotEmpty = {time} name={name} label={label} iconName={iconName} passIconName={passIconName}>
      <TimePicker

        defaultValue= {`${defaultValue} am`}
        placeholder=" "
        useTwelveHourFormat={true}
        showCloseIcon={false}
        showClockIcon={false}
        onTimeChange={onTimeChange}
        {...props} 
      />
    </FormFieldLayout>
  );
};

export default CustomTimePicker;
