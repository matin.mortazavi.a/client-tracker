import React, { useEffect, useState } from 'react';
import FormFieldLayout from '../../layouts/formFeildsLayout/FormFieldLayout';
import '@hassanmojab/react-modern-calendar-datepicker/lib/DatePicker.css';
import DatePicker from '@hassanmojab/react-modern-calendar-datepicker';
import './custom.scss';
import jalaliMoment from 'jalali-moment';
import moment from 'jalali-moment';

export default function CustomDatePicker({ name, label,defaultValue="", iconName, onChange }) {

  const [selectedDay, setSelectedDay] = useState(null);
 
  
  const formatInputValue = () => {
    if (!selectedDay) return '';
    const { day, month, year } = selectedDay;
    return `${day}:${month}:${year}`;
  };
  const onChangeHandler=(value)=>{
    const miladiDate =moment.from(`${value.year}/${value.month}/${value.day}`, 'fa', 'YYYY/MM/DD').format('YYYY/MM/DD'); 
    const date = new Date(miladiDate).setHours(12,0,0,0)
    onChange(date);
    setSelectedDay(value);
  }

  return (
    <FormFieldLayout name={name} label={label} iconName={iconName}>
      <DatePicker
        value={selectedDay} 
        onChange={onChangeHandler}
        inputPlaceholder=""
        formatInputText={formatInputValue}
        inputClassName="my-custom-input"
        locale="fa"
        shouldHighlightWeekends
      />
    </FormFieldLayout>
  );
}
