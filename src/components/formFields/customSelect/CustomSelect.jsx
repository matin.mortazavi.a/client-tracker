import React from 'react';

import FormFieldLayout from '@components/layouts/formFeildsLayout/FormFieldLayout';

import styles from "./CustomSelect.module.scss";
import { forwardRef } from 'react';

const CustomSelect = forwardRef(({value , name , label,errors,iconName,passIconName,...props },ref) => {
  return (
    <FormFieldLayout isNotEmpty = {value} name = {name} label = {label} errors = {errors} iconName = {iconName} passIconName = {passIconName} >
     <select  className={styles['select']}
     {... props.inputdetails}
     {... props}
     

>
  <option value=""></option>
{
  props.dropDownList?.map((item)=>(
    <option value={item.id}>
      {item.activityType}
    </option>
  ))
}
</select>
    </FormFieldLayout>
  );
});

export default CustomSelect;
