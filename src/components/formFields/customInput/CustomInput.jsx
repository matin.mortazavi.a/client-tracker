import React from 'react';
import FormFieldLayout from '@components/layouts/formFeildsLayout/FormFieldLayout';
import styles from "./CustomInput.module.scss";
import { forwardRef } from 'react';

const CustomInput = forwardRef(({value, name ,handlePasswordVisibility, label,errors,iconName,passIconName,...props },ref) => {

  return (
    <FormFieldLayout handlePasswordVisibility = {handlePasswordVisibility} isNotEmpty ={value}  name = {name} label = {label} errors = {errors} iconName = {iconName} passIconName = {passIconName} >
     <input className={styles['input']}
     {... props.inputdetails}
     {... props}
     

/>
    </FormFieldLayout>
  );
});

export default CustomInput;
