import React from 'react'
import { forwardRef } from 'react';
import FormFieldLayout from '@components/layouts/formFeildsLayout/FormFieldLayout';
import styles from "../customTextarea/CustomTextarea.module.scss"

 


const CustomTextarea = forwardRef(({value, name , label,errors,iconName,passIconName,...props },ref) => {
  return (
    <FormFieldLayout isNotEmpty = {value} name = {name} label = {label} errors = {errors} iconName = {iconName} passIconName = {passIconName} >
     <textarea  className={styles['textarea']}
     {... props.inputdetails}
    {... props}
    cols={40}
      rows={4}

/>
    </FormFieldLayout>
  );
});

export default CustomTextarea;