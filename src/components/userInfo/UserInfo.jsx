import React, { useEffect } from 'react'
import styles from "./UserInfo.module.scss"
import  profile from "@/assets/images/profile.png"
import { useUserContext } from '@/context/UserContext'
import SvgLoader from '../SvgLoader'


export default function UserInfo() {
  const {getUserInfo,user} = useUserContext()
  useEffect(()=>{
    getUserInfo()
  },[])



  
 
  return (
    <div className={styles['user-info']}>
        <div className={styles.container}>
        <div className={styles['user-info-content']}>
            <img className={styles["user-profile"]} src={profile} alt="" />
            <div className={styles["user-details"]}>
                <p className={styles["user-name"]}> {user?.name} </p>
                <span className={styles["user-events-count"]}>بدون رویداد</span>
            </div>

        </div>
        <SvgLoader className={styles['user-notif-icon']} name="notif" ></SvgLoader>
        </div>
        
        
       
    </div>
  )
}
