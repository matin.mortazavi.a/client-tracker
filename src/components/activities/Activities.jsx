



import React, { useEffect,useState } from 'react'
import styles from "./Activities.module.scss"
import {ActivityItem,Alert} from '@components/Common'
import { useActivitesContext } from '@/context/ActivitesContext'
export default function Activities() {
  
  const {getActivities,activities} = useActivitesContext()
  const [showAlert, setShowAlert] = useState(false);
  
  useEffect(()=>{
    try{
      getActivities()
    }catch(error){
      setShowAlert(true);
      setTimeout(() => {
          setShowAlert(false);
      }, 2000);
    }
  },[])

  return (
    <div className={styles["activity"]}>
      {showAlert && (
    <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
    )}  
    <div className={styles["activity-details"]}>
      <p className={styles["activity-title"]}>  نوع فعالیت</p>
      
    
    </div>
   
    <div className={styles["activity-items"]}>
    
    {
      activities.length ?  activities.slice(0,4).map((activity,index)=>(
      
        <ActivityItem key = {index} {...activity} ></ActivityItem>
      ))
      : (
       <>
        <ActivityItem></ActivityItem>
        <ActivityItem></ActivityItem>
        <ActivityItem></ActivityItem>
        <ActivityItem></ActivityItem>
        </>
      )
     
    }
    
    </div>


  </div>
  )
}
