import React, { useEffect, useState } from 'react';
import styles from "./EventForm.module.scss"
import { useForm } from 'react-hook-form';
import CustomButton from '@components/Common/customButton/CustomButton';
import CustomTextarea from '@components/formFields/customTextarea/CustomTextarea';
import CustomInput from '@components/formFields/customInput/CustomInput';
import CustomTimePicker from '@components/formFields/customTimePicker/CustomTimePicker';
import CustomDatePicker from '@components/formFields/customDatePicker/CustomDatePicker';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useEventsContext } from '@/context/EventsContext';
export default function EventForm({closePopup = () => {}, ...props}) {
  const {addEvent,editEvent} = useEventsContext()
    const [time, setTime] = useState('');
    const [date, setDate] = useState('');
    const { register, handleSubmit, watch, reset, formState: { errors } } = useForm({
      mode: 'onBlur',
    });
  
  
    const handletimeChange = (value) => {
      setTime(value);
    };
  
  
    const handleDateChange = (value) => {
      setDate(value);
    };
  
    const onSubmit = async (data) => {
      let existingDate = new Date(date);
      const [hours, minutes] = time.split(":");
      existingDate.setHours(hours)
      existingDate.setMinutes(minutes);
      const timeStamp = existingDate.getTime()
      try {
        const allFormData = {
          ...data,
          date :timeStamp,
        };

        if (props.mode == 'edit') {
            await editEvent(allFormData,props.id );
        } else {
         await addEvent(allFormData);
        }
        reset();
        closePopup();
      } catch (error) {
        console.error('Error in onSubmit:', error);
      }
    };
  
    const inputsDetails = [
      {
        id: 1,
  
        name: 'title',
        label: 'نام فعالیت',
        iconName: 'popUpNote',
        type: 'text',
        errors: errors['title'],
        value: watch('title'),
        rules: { required: 'این فیلد الزامی است' },
        defaultValue: props.defaultValues?.title || null,
      },
      {
        id: 2,
        name: 'description',
        label: 'توضیحات',
        iconName: 'copy',
        type: 'textarea',
        errors: errors['description'],
        value: watch('description'),
        defaultValue: props.defaultValues?.description || "",
      },
      {
        id: 3,
        name: 'date',
        label: 'تاریخ',
        iconName: 'calendar',
        onChange: handleDateChange,
        defaultValue: props.defaultValues?.date || "",
      },
      {
        id: 4,
        name: 'time',
        label: 'از ساعت',
        iconName: 'clock',
        time: time,
        rules: { required: 'این فیلد الزامی است' },
        onChange: handletimeChange,
        defaultValue: props.defaultValues?.time || "",
      },
   

    ];
   
  
    return (
    
      <form onSubmit={handleSubmit(onSubmit)}>
          <div className={styles['popup']}>
           <div className={styles['popup__header']}>
           <ArrowForwardIcon  onClick={closePopup} className={styles['header__icon']} />
           <span className={styles['header__title']}>
            ایجاد رویداد
            </span>
           </div>
           
     
      <div className={styles['popup__inputs']}>
      {
    inputsDetails.map((input) => (
      input.type === "textarea" && (
        <CustomTextarea 
          inputdetails={{...register(input.name, input.rules) }}
          {...input}
          key={input.id}
        />
      )
      ||
      (input.name === "date" && (
        <CustomDatePicker {...input} key={input.id} />
      ))
      ||
      ((input.name === "time") && (      
          <CustomTimePicker {...input}></CustomTimePicker>
      ))
      ||
      (
        <CustomInput
          inputdetails={{...register(input.name, input.rules) }}
          {...input}
          key={input.id}
        />
      )
    ))
    
     
    }
     
      </div>
      <div className={styles['popup__btns']}>
      
    
      <div className={styles['popup--half']}>
        <CustomButton type='button' color='secondary' text={"لغو "} onClick = {closePopup}></CustomButton>
        
      </div>
      <div className={styles['popup--half']}>
        <CustomButton text={"تایید"}></CustomButton>
        
      </div>
      </div>
      
         
    
        </div>
      </form>
      )
}
