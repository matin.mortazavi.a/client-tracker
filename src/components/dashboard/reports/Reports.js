import React from "react";
import styles from "./Reports.module.scss";
import {ActivityCount,MostActivities,SummeryReport} from "@components/Common"
export default function Reports() {
    return (
        <div className={styles["reports"]}>
            <div className={styles["reports-details"]}>
                <p className={styles["reports-title"]}>گزارش ها</p>
                <div className={styles["reports-filtering"]}>
                    <span className={styles["reports-filtering__item"]} href="">
                        روزانه
                    </span>
                    <span className={styles["reports-filtering__item"]} href="">
                        هفتگی
                    </span>
                    <span className={styles["reports-filtering__item"]} href="">
                        ماهانه
                    </span>
                    <span className={styles["reports-filtering__item"]} href="">
                        سالانه
                    </span>
                </div>
            </div>

            <div className={styles["items"]}>
                <div className={`${styles["report-item"]} ${styles["report-item__activity-count"]}` }>
                    <ActivityCount></ActivityCount>
                </div>

                <div className={`${styles["report-item"]} ${styles["report-item__most-activites"]}` }>
                    <MostActivities></MostActivities>
                </div>
                <div className={`${styles["report-item"]} ${styles["report-item__summery-report"]}` }>
                    <SummeryReport></SummeryReport>
                </div>
            </div>

            <div className={styles["events"]}></div>
        </div>
    );
}
