
import React, { useEffect } from 'react'
import styles from "./ActivityTypes.module.scss"
import {ActivityTypeItem} from '@components/Common'
import { useActivitesTypeContext } from '@/context/ActivitesTypeContext'
export default function ActivityTypes() {
  
  const {getActivityTypesList,activityTypesList} = useActivitesTypeContext()
  useEffect(()=>{
    getActivityTypesList()
  },[])
  return (
    <div className={styles["events"]}>
    <div className={styles["events-details"]}>
      <p className={styles["events-title"]}>  نوع فعالیت</p>
    
    </div>
   
    <div className={styles["event-items"]}>
    
    {
      activityTypesList.length ?  activityTypesList.slice(0,4).map((activityType,index)=>(
      
        <ActivityTypeItem key = {index} {...activityType} ></ActivityTypeItem>
      ))
      : (
       <>
        <ActivityTypeItem></ActivityTypeItem>
        <ActivityTypeItem></ActivityTypeItem>
        <ActivityTypeItem></ActivityTypeItem>
        <ActivityTypeItem></ActivityTypeItem>
        </>
      )
     
    }
    
    </div>


  </div>
  )
}
