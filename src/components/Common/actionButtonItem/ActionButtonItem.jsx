import React from 'react'
import  "./ActionButtonItem.scss"
import SvgLoader from '@components/SvgLoader';
export default function ActionButtonItem(props) {
  return (
    <div  className="sub-menu__container"  onClick={props.clickHandler}  >
    <div className="sub-menu__img-container">
    
      <SvgLoader className="sub-menu__item-icon" name ={props.img}>
        
      </SvgLoader>
    </div>
    <span className="sub-menu__item-text">{props.name}</span>
  </div>
  )
}
