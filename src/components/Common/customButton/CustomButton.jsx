import React from 'react';
import styles from "./CustomButton.module.scss";

export default function CustomButton({ onClick = () => {}, type = "submit", color = "primary", ...props }) {

  return (
    <button onClick={onClick} className={`${styles["custom-btn"]} ${styles[`${color}`]}`} type={type} {...props}>
      {props.text}
    </button>
  );
}
