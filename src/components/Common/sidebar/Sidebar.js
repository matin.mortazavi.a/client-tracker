import React from "react";
import styles from "./Sidebar.module.scss";

import { NavLink } from "react-router-dom";
import SvgLoader from "@components/SvgLoader";
import "./custom.scss";
export default function SideBar() {
    return (
        <div className={styles["sidebar"]}>
            <div className={styles["sidebar__icons"]}>
                <div className={styles["icons__section"]}>
       
                    <NavLink className="not-active" to={"/"}>
                        <SvgLoader
                            name="home"
                            className={styles["sidebar__icon"]}
                        />
                    </NavLink>
                    <NavLink className="not-active" to={"/events"}>
                        <SvgLoader
                            name="sidebarFlash"
                            className={styles["sidebar__icon"]}
                        />
                    </NavLink>
                </div>

                <div className={styles["icons__section"]}>
                    <NavLink className="not-active" to={"/activities"}>
                        <SvgLoader
                            name="note"
                            className={styles["sidebar__icon"]}
                        />
                    </NavLink>
                    <NavLink className={styles['mobile-only']}>
                        <SvgLoader
                            name="userSidebar"
                            className={styles["sidebar__icon"]}
                        />
                    </NavLink>
                </div>
            </div>
        </div>
    );
}
