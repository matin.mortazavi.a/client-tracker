import React from 'react'
import styles from"./container.module.scss"
export default function Alert(props) {
  
    
   
   
  return (
    <>
    <div className= {`${styles.container} ${styles.error}`}>
        <h2 className= {styles.title} >{props.title}</h2>
        <p className= {styles.text}>{props.text}</p>
     
      </div>
    <div className={styles['backdrop']}></div>
    </>
  )
}
