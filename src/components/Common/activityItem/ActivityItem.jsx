import React, { useEffect, useState } from 'react';
import styles from "./ActivityItem.module.scss";
import SvgLoader from '@components/SvgLoader';
import  {useActivitesTypeContext}  from '@/context/ActivitesTypeContext';
import ActivityMethodes from '@components/activityMethodes/ActivityMethodes';
import ImgSlider from '@components/Common/imgSlider/ImgSldier';
import jalaliMoment from 'jalali-moment';
import getPersianDateService from '@/services/getPersianDateService';
export default function ActivityItem(props) {
  const {activityTypesList,getActivityTypesList} = useActivitesTypeContext()
  const activityType = activityTypesList.find(i => i.id == props.activityId)
  useEffect(()=>{
    getActivityTypesList()
  },[])
 

 
 
let date = null
if(props?.title){
  const {jalaliDay, persianMonth} = getPersianDateService(props?.date);

  date = `${jalaliDay} ${persianMonth} `
}
   
  

 
  const [showActivityMethodes, setShowActivityMethodes] = useState(false);
  return (
    <>
      {props?.title ? (
        <div className={styles["activity-item"]}>
      {showActivityMethodes && 
        <ActivityMethodes activityType = {props.activityId} activityDetails={props} showActivityMethodes={setShowActivityMethodes} />
      }
        <div className={styles['activity-item-filled']}>
          <div className={styles['activity-item-filled__img-wrapper']}>
          <ImgSlider images={props.image} />
          </div>
          <div className={styles['activity-item-filled__details']}>
            <p className={styles['details__activity-name']}>{props.title}</p>
            <span className={styles['details__activity-icon']} onClick={() => setShowActivityMethodes(true)}>...</span>
          </div>
          <p className={styles['activity-item-filled__description']}>{props.description}</p>
          <div className={styles['activity-item-filled__line']}></div>
          <div className={styles['activity-item-filled__date']}>
            <div className={styles['date__time']}>
              <SvgLoader className={styles['date-time__icon']} name="calendar"></SvgLoader>
              <span className={styles['date-time__text']}>{date}</span>
            </div>
            <div className={styles['date__time']}>
              <SvgLoader className={styles['date-time__icon']} name="clock"></SvgLoader>
              <span className={styles['date-time__text']}>{props.startTime} تا {props.endTime}</span>
            </div>
          </div>
          <span className={styles['activity-item-filled__activity-type']}>
            {activityType?.activityType}
          </span>
        </div>
        </div>
      ) : (
        <div className={`${styles["activity-item"]} ${styles["activity-item--empty"]}`}>
          <SvgLoader name="plus"/>
        </div>
      )}
      </>
  );
}
