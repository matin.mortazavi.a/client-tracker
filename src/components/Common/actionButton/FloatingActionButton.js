import React,{Suspense, lazy, useMemo} from "react";
import { useState } from "react";
import "./FloatingActionButton.scss"
const  ActionButtonItem = lazy(()=>import("@components/Common/actionButtonItem/ActionButtonItem"))
export default function FloatingActionButton(props) {

    const [isRotated, setIsRotated] = useState(false);

    const rotatedClass = useMemo(() => {
        return isRotated ? "rotated" : "";
    }, [isRotated]);
    const showEventPopupFunc = (event) => {
        event.preventDefault();
        props.setShowEventPopup((prevState) => !prevState)
        event.stopPropagation();
    };
    const showActivityTypePopupFunc = (event) => {
        event.preventDefault();
        props.setShowActivityTypePopup((prevState) => !prevState)


        event.stopPropagation();
    };
    const showActivityPopupFunc = (event) => {
        event.preventDefault();

        props.setShowActivityPopup((prevState) => !prevState);

        event.stopPropagation();
    };

    const subMenus = [
        { id: 1, name: "رویداد", img: "flash",handler: showEventPopupFunc  },
        {
            id: 2,
            name: "نوع فعالیت",
            img: "note",
            handler: showActivityTypePopupFunc
        },
        { id: 3, name: "فعالیت", img: "weight", handler: showActivityPopupFunc }
    ];
    const floatingActionHandler = (event) => {
        event.preventDefault();
        props.setShowActivityPopup(false);
        props.setShowActivityTypePopup(false);

        setIsRotated((prevState) => !prevState);
        event.stopPropagation();
    };

    return (
        <>
            {isRotated && <div className="backdrop"></div>}
            <div className={`action-button-wrapper ${rotatedClass}`}>
                <div
                    className={`floating-action ${rotatedClass}`}
                    onClick={(event) => floatingActionHandler(event)}
                >
                    <span className="floating-action__item floating-action__item--rotated">
                        |
                    </span>
                    <span className="floating-action__item">|</span>
                    <div className="floating-action__sub-menu">
                        {subMenus?.map((item) => (
                            <Suspense fallback = {<div>loading...</div>}>
                                <ActionButtonItem
                                key={item.id}
                                name={item.name}
                                img={item.img}
                                clickHandler={item.handler}
                            />
                            </Suspense>
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
}
