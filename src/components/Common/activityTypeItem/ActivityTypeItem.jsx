import React, { useState } from 'react'
import styles from "./ActivityItem.module.scss"
import SvgLoader from '@components/SvgLoader'
import ActivityTypeMethodes from "@components/activityTypeMethodes/ActivityTypeMethodes";

export default function ActivityTypeItem(props) {
  const [showActivityTypeMethodes,setShowActivityTypeMethodes] = useState(false)
  return (
    
    <>
   {
    props.activityType? (
      <div className={styles["activity-type-item"]}>
      {showActivityTypeMethodes && 
      <ActivityTypeMethodes activityTypeDetails = {props} showActivityTypeMethodes ={setShowActivityTypeMethodes} />
      }
     <div className={styles['activity-type-item-filled']}>
      <div className={styles['activity-type-item-filled__name-section']}>
        <p  className={styles['name-section__name']}>{props.activityType}</p>
        <span onClick={()=>setShowActivityTypeMethodes(true)} className={styles['name-section__icon']}>...</span>
      </div>
      <div className={styles['activity-type-item-filled__count-section']}>
       
          <SvgLoader className={styles['count-section__icon']} name = "note"></SvgLoader>

        <span  className={styles['count-section__count']}>
          
        {props.activitiesCount} فعالیت 
        </span>
      </div>
     </div>
     </div>

    ) :
    (
      <div className={`${styles["activity-type-item"]} ${styles["activity-type-item--empty"]}  `}>
      <SvgLoader name="plus"/>
      </div>
    )
   }
    </>
  )
}
