import React from 'react'
import styles from "./ActivityCount.module.scss"
import emptyReport from "@/assets/SVGs/emptyreports.svg";
import SvgLoader from '../../SvgLoader';
export default function ActivityCount() {
  return (
 
         <><div className={styles["reports-item"]}>
          <SvgLoader className={styles["reports-icon"]} name="emptyreports"></SvgLoader>
        
         <span className={styles["reports-icon__number"]}>‍۱</span>
        
         
</div>
<span className={styles["reports-item__text"]}>بدون فعالیت</span></>

   
  )
}
