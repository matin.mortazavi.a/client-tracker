// ImageSlider.js
import React, { useState } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import styles from './ImgSlider.module.scss';

const ImgSlider = ({ images }) => {
  const [currentSlide, setCurrentSlide] = useState(0);

  const nextSlide = () => {
    setCurrentSlide((prevSlide) => (prevSlide + 1) % images.length);
  };

  const prevSlide = () => {
    setCurrentSlide((prevSlide) => (prevSlide - 1 + images.length) % images.length);
  };
  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <div className={styles['custom-next-arrow']} onClick={nextSlide}>&rarr;</div>,
    prevArrow: <div className={styles['custom-prev-arrow']} onClick={prevSlide}>&larr;</div>,
    afterChange: (index) => setCurrentSlide(index),
  };

 

  return (
    <div className={styles['image-slider']}>
      <Slider {...settings}>
        {images?.map((image, index) => (
          <img
            key={index}
            src={image}
            alt={`Image ${index + 1}`}
            className={styles['slider-image']}
          />
        ))}
      </Slider>
    </div>
  );
};

export default ImgSlider;
