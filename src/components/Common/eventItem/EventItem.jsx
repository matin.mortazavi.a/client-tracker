import React,{useState} from 'react'
import styles from "./EventItem.module.scss"
import SvgLoader from "@components/SvgLoader"
import EventMethodes from '@components/eventMethodes/EventMethodes';
import getPersianDateService from '@/services/getPersianDateService';
export default function EventItem(props) {
  
let date = null
let time = null

if(props?.title){
  const { persianMonthIndex, jalaliDay, persianMonth, hour, minutes } = getPersianDateService(props?.date);
  // const miladiDate = new Date(props?.date).toLocaleDateString("en-US")
  // const splitedDate = miladiDate.split("/")
  // const year = parseInt(splitedDate[2], 10);
  // const month = parseInt(splitedDate[0], 10);
  // const day = parseInt(splitedDate[1], 10);
  // const persianDate = moment(`${year}/${month}/${day}`, 'YYYY/MM/DD').locale('fa').format('YYYY/MM/DD'); // 1367/11/04
  // const persianDateSplited = persianDate.split("/")
  // const persianMonthIndex = persianDateSplited[1]
  // const jalaliDay = persianDateSplited[2]
  // const persianMonth = persianMonths[persianMonthIndex - 1];
  // const hour = new Date(props?.date).getHours()  
  // const minutes = new Date(props?.date).getMinutes(); 
  time = `${hour}:${minutes}`;
  date = ` ${jalaliDay} ${persianMonth} `
}
   
  

 
  const [showEventMethodes, setShowEventMethodes] = useState(false);
  
  return (
    <>
      {props?.title ? (
        <div className={styles["event-item"]}>
      {showEventMethodes && 
        <EventMethodes eventDetails={props} showEventMethodes={setShowEventMethodes} />
       }
        <div className={styles['event-item-filled']}>
         
          <div className={styles['event-item-filled__details']}>
            <p className={styles['details__event-name']}>{props.title}</p>
            <span className={styles['details__event-icon']} onClick={() => setShowEventMethodes(true)}>...</span>
          </div>
          <p className={styles['event-item-filled__description']}>{props.description}</p>
          <div className={styles['event-item-filled__line']}></div>
          <div className={styles['event-item-filled__date']}>
            <div className={styles['date__time']}>
              <SvgLoader className={styles['date-time__icon']} name="calendar"></SvgLoader>
              <span className={styles['date-time__text']}>{date}</span>
            </div>
            <div className={styles['date__time']}>
              <SvgLoader className={styles['date-time__icon']} name="clock"></SvgLoader>
              <span className={styles['date-time__text']}>{time}</span>
            </div>
          </div>
        
        </div>
        </div>
      ) : (
        <div className={`${styles["event-item"]} ${styles["event-item--empty"]}`}>
          <SvgLoader name="plus"/>
        </div>
      )}
      </>
  );

}
