import React, { useState } from 'react'
import styles from "./ActivityMethodes.module.scss"
import DeleteActivity from '@components/deleteActivity/DeleteActivity'
import CustomButton from "@components/Common/customButton/CustomButton"
import ActivityForm from '../activityForm/ActivityForm'
export default function ActivityMethodes({showActivityMethodes=()=>{},...props}) {
    const [showDeletePopup,setShowDeletePopup] = useState(false)
    const [showEditPopup,setShowEditPopup] = useState(false)

   
    const closePopups=()=>{
        setShowDeletePopup(false)
        setShowEditPopup(false)
      
    }
  return (
    <>
    <div className={styles['backdrop']}>
    </div>
    <div className={styles['pop-up']}>
    {showDeletePopup && (
        <DeleteActivity closePopup = {closePopups} activityType = {props.activityType} activityID = {props.activityDetails._id} > </DeleteActivity>
    )}
    {showEditPopup && (

    <ActivityForm mode = "edit" closePopup = {closePopups} defaultValues = {props.activityDetails} id = {props.activityDetails._id} ></ActivityForm>        

    )}
  
    <CustomButton onClick={()=>setShowEditPopup(true)} text="ویرایش"></CustomButton>
    <CustomButton onClick={()=>setShowDeletePopup(true)} text="حدف"></CustomButton>
    <CustomButton onClick={()=>showActivityMethodes(false)} text="بستن"></CustomButton>
    </div></>
  )
}
