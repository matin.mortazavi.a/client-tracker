import React from "react";
import userIcon from "@/assets/SVGs/user-icon.svg";
import lock from "@/assets/SVGs/lock.png";
import eye from "@/assets/SVGs/eye-slash.svg";
import calendar from "@/assets/SVGs/calendar.svg";
import flash from "@/assets/SVGs/flash-side.svg";
import sidebarFlash from "@/assets/SVGs/flash.svg";
import note from "@/assets/SVGs/note.svg";
import note2 from "@/assets/SVGs/note-side.svg";
import weight from "@/assets/SVGs/weight-side.svg";
import home from "@/assets/SVGs/home.svg";
import plus from "@/assets/SVGs/plus.svg";
import popUpWeight from "@/assets/SVGs/pop-up-weight.svg";
import popUpNote from "@/assets/SVGs/pup-up-note.svg";
import copy from "@/assets/SVGs/document-copy.svg";
import clock from "@/assets/SVGs/clock.svg";
import notif from "@/assets/SVGs/notification-bing.svg";
import emptyreports from "@/assets/SVGs/emptyreports.svg";
import userSidebar from "@/assets/SVGs/user-sidebar.svg"


const svgMap = {
    userIcon,
    lock,
    eye,
    calendar,
    flash,
    note,
    weight,
    sidebarFlash,
    home,
    plus,
    note2,
    clock,
    copy,
    popUpNote,
    popUpWeight,
    notif,
    emptyreports,
    userSidebar
};

const SvgLoader = ({ onClick, className, name }) => {
    const selectedSvg = svgMap[name];

    return <img onClick={onClick} className={className} src={selectedSvg} />;
};

export default SvgLoader;
