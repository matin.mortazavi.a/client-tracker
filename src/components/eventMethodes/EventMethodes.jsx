import React, { useState } from 'react'
import styles from "./EventMethodes.module.scss"
import EventForm from '@components/eventForm/EventForm'
import CustomButton from "@components/Common/customButton/CustomButton"
import DeleteEvent from '@components/deleteEvent/DeleteEvent'
export default function EventMethodes({showEventMethodes=()=>{},...props}) {
    
    const [showDeletePopup,setShowDeletePopup] = useState(false)
    const [showEditPopup,setShowEditPopup] = useState(false)

   
    const closePopups=()=>{
        setShowDeletePopup(false)
        setShowEditPopup(false)
      
    }
  return (
    <>
    <div className={styles['backdrop']}>
    </div>
    <div className={styles['pop-up']}>
    {showDeletePopup && (
        <DeleteEvent closePopup = {closePopups} id = {props.eventDetails.id}></DeleteEvent>
    )}
    {showEditPopup && (
        <EventForm closePopup = {closePopups} mode = "edit" defaultValues = {props.eventDetails} id = {props.eventDetails.id}></EventForm>
    )}
   
    <CustomButton onClick={()=>setShowEditPopup(true)} text="ویرایش"></CustomButton>
    <CustomButton onClick={()=>setShowDeletePopup(true)} text="حدف"></CustomButton>
    <CustomButton onClick={()=>showEventMethodes(false)} text="بستن"></CustomButton>
    </div></>
  )
}
