import React from 'react'
import styles from "./DeleteEvent.module.scss"
import CustomButton from "@components/Common/customButton/CustomButton"
import {Alert} from "@components/Common"
import { useState } from 'react'
import { useEventsContext } from '@/context/EventsContext'
export default function DeleteEvent({closePopup=()=>{},...props}) {
    const [showAlert, setShowAlert] = useState(false);
    const {deleteEvent} = useEventsContext()
    const onClickHandler = async()=>{
       
        try{
            await deleteEvent(props.id)
            closePopup()
        }catch(error){
            setShowAlert(true);
            setTimeout(() => {
              setShowAlert(false);
            }, 2000);
        }
       
    }
  return (

    <>
    <div className={styles['backdrop']}>
        
    </div>
    <div className={styles['pop-up']}>
    {showAlert && (
        <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
      )}

    <CustomButton text={"تایید"}  onClick={onClickHandler}></CustomButton>
    <CustomButton type='button' color='secondary' text={"لغو "} onClick = {closePopup}></CustomButton>
        
        
    </div></>
  )
}
