import React from 'react'
import styles from "./DeleteActivityType.module.scss"
import { useActivitesTypeContext } from '@/context/ActivitesTypeContext'
import CustomButton from "@components/Common/customButton/CustomButton"
import {Alert} from "@components/Common"
import { useState } from 'react'
export default function DeleteActivityType({closePopup=()=>{},...props}) {
    const [showAlert, setShowAlert] = useState(false);
    const {deleteActivityType} = useActivitesTypeContext()
    const onClickHandler = async()=>{
       
        try{
            await deleteActivityType(props.id)
            closePopup()
        }catch(error){
            setShowAlert(true);
            setTimeout(() => {
              setShowAlert(false);
            }, 2000);
        }
       
    }
  return (

    <>
    <div className={styles['backdrop']}>
        
    </div>
    <div className={styles['pop-up']}>
    {showAlert && (
        <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
      )}

    <CustomButton text={"تایید"}  onClick={onClickHandler}></CustomButton>
    <CustomButton type='button' color='secondary' text={"لغو "} onClick = {closePopup}></CustomButton>
        
        
    </div></>
  )
}
