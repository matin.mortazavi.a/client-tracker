import React from 'react'
import styles from "./NewActivityType.module.scss"
import {useForm} from "react-hook-form"
import CustomButton from '@components/Common/customButton/CustomButton'
import CustomInput from '@components/formFields/customInput/CustomInput'
import {Alert} from "@components/Common"
import { useState } from 'react'
import { useActivitesTypeContext } from '@/context/ActivitesTypeContext'
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

export default function NewActivityType({closePopup=()=>{},...props}) {
  const [showAlert, setShowAlert] = useState(false);
  const {addActivityType} =useActivitesTypeContext() 
    const {
        register,
        handleSubmit,
        watch,
        reset,
        formState: { errors },
      } = useForm({
        mode: "onBlur",
      });
      const inputsDetails = [
        {
          id: 1,
          name: "activityType",
          label: "نام فعالیت",
          iconName: "popUpNote",
          type: "text",
          errors: errors["activityType"],
          value : watch("activityType"),
          rules: { required: "این فیلد الزامی است" },
        
        }];
     const onSubmit = async(data)=>{
       try{
        await addActivityType(data)
        closePopup()
       }catch(error){
          setShowAlert(true);
      setTimeout(() => {
        setShowAlert(false);
      }, 2000);
       }
       reset()  
      }

  return (
    
    <div className={styles['popup']}>
       {showAlert && (
        <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
      )}
         <div className={styles['popup__header']}>
           <ArrowForwardIcon  onClick={closePopup} className={styles['header__icon']} />
           <span className={styles['header__title']}>
            ایجاد رویداد
            </span>
           </div>
       
  <form onSubmit={handleSubmit(onSubmit)}>
  <div className={styles['popup__inputs']}>
 {
  inputsDetails.map((input) => (
    <CustomInput inputdetails={{...register(input.name, input.rules) }} {...input} />
  ))
  
 }
  </div>
  <div className={styles['popup__btns']}>
  

  <div className={styles['popup--half']}>
    <CustomButton type='button' color='secondary' text={"لغو "} onClick = {closePopup}></CustomButton>
    
  </div>
  <div className={styles['popup--half']}>
    <CustomButton text={"تایید"}></CustomButton>
    
  </div>
  </div>
  
    </form> 

     

    </div>
  )
}
