import React,{useEffect,useState} from 'react'
import styles from "./Events.module.scss"
import {Alert} from "@components/Common"
import EventItem from '@components/Common/eventItem/EventItem'
import { useEventsContext } from '@/context/EventsContext'
export default function Events() {

  const {getEvents,events} = useEventsContext()
  const [showAlert, setShowAlert] = useState(false);

  useEffect(()=>{
    try{
      getEvents()
    }catch(error){
      setShowAlert(true);
      setTimeout(() => {
          setShowAlert(false);
      }, 2000);
    }
   
  }
  ,[])

  return (
    <div className={styles["event"]}>
      {showAlert && (
    <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
    )}  
    <div className={styles["event-details"]}>
      <p className={styles["event-title"]}>رویداد ها </p>
      
    
    </div>
   
    <div className={styles["event-items"]}>
    
    {
      events.length ?  events.slice(0,3).map((event)=>(
      
        <EventItem {...event} ></EventItem>
      ))
      : (
       <>
        <EventItem></EventItem>
        <EventItem></EventItem>
        <EventItem></EventItem>
       
        </>
      )
     
    }
    
    </div>


  </div>
  )
}
