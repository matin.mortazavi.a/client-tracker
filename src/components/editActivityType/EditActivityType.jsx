import React from 'react'
import styles from "./EditActivityType.module.scss"
import {useForm} from "react-hook-form"
import CustomButton from '@components/Common/customButton/CustomButton'
import CustomInput from '@components/formFields/customInput/CustomInput'
import { useActivitesTypeContext } from '@/context/ActivitesTypeContext'
import {Alert} from "@components/Common"

import { useState } from 'react'
export default function EditActivityType({closePopup=()=>{},...props}) {

  const [showAlert, setShowAlert] = useState(false);
  const {editActivityType} = useActivitesTypeContext() 
    const {
        register,
        handleSubmit,
        watch,
        reset,
        formState: { errors },
      } = useForm({
        mode: "onBlur",
      });
      const inputsDetails = [
        {
          id: 1,
          name: "activityType",
          label: "نام نوع فعالیت",
          iconName: "popUpNote",
          type: "text",
          errors: errors["activityType"],
          value : watch("activityType"),
          rules: { required: "این فیلد الزامی است" },
          defaultValue : props.defaultValue
        
        },
       
          
        
        

      ];
     const onSubmit = async(data)=>{
       try{
        await editActivityType(props.id,data)
        closePopup()
       }catch(error){
          setShowAlert(true);
      setTimeout(() => {
        setShowAlert(false);
      }, 2000);
       }
       reset()
        
      }

  return (
    
    <div className={styles['pop-up']}>
       {showAlert && (
        <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
      )}
        <span className={styles['pop-up__title']}>
        ویرایش نوع فعالیت
        </span>
       
  <form onSubmit={handleSubmit(onSubmit)}>
  <div className={styles['popup-inputs']}>
 {
  inputsDetails.map((input) => (
    <CustomInput inputdetails={{...register(input.name, input.rules) }} {...input} />
  ))
  
 }
  </div>
  <div className={styles['popup-btns']}>
  

  <div className={styles['popup--half']}>
    <CustomButton type='button' color='secondary' text={"لغو "} onClick = {closePopup}></CustomButton>
    
  </div>
  <div className={styles['popup--half']}>
    <CustomButton text={"تایید"}></CustomButton>
    
  </div>
  </div>
  
    </form> 

     

    </div>
  )
}
