import React, { useEffect, useState } from 'react';
import styles from "./ActivityForm.module.scss"
import { useForm } from 'react-hook-form';
import CustomButton from '@components/Common/customButton/CustomButton';
import CustomTextarea from '@components/formFields/customTextarea/CustomTextarea';
import CustomInput from '@components/formFields/customInput/CustomInput';
import CustomTimePicker from '@components/formFields/customTimePicker/CustomTimePicker';
import CustomDatePicker from '@components/formFields/customDatePicker/CustomDatePicker';
import CustomImageUploader from '@components/formFields/customImageUploader/CustomImageUploader';
import CustomSelect from '@components/formFields/customSelect/CustomSelect';
import { useActivitesContext } from '@/context/ActivitesContext';
import { useActivitesTypeContext } from '@/context/ActivitesTypeContext';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

export default function ActivityForm({ closePopup = () => {}, ...props }){
  const { addActivity, editActivity } = useActivitesContext();
  const { getActivityTypesList, activityTypesList } = useActivitesTypeContext();
  const [startTime, setStartTime] = useState('');
  const [endTime, setEndTime] = useState('');
  const [date, setDate] = useState('');
  const [image, setImage] = useState(props.defaultValues?.image || []);
  const { register, handleSubmit, watch, reset, formState: { errors } } = useForm({
    mode: 'onBlur',
  });

  useEffect(() => {
    getActivityTypesList();
  }, []);

  const handleImageChange = async (id, file) => {
    const reader = new FileReader();
    const base64String = await new Promise((resolve) => {
      reader.onloadend = function () {
        const result = reader.result;
        resolve(result);
      };

      reader.readAsDataURL(file);
    });

    const newImages = [...image];
    newImages[id] = base64String;

    setImage(newImages.filter(img => img !== null && img !== undefined));
  };

  const handleStartTimeChange = (value) => {
    setStartTime(value);
  };

  const handleEndTimeChange = (value) => {
    setEndTime(value);
  };

  const handleDateChange = (value) => {
    setDate(value);
  };

  const onSubmit = async (data) => {
    try {
      const allFormData = {
        ...data,
        startTime,
        endTime,
        date,
      };

      const { activityType, ...newObj } = allFormData;

      newObj.image = image;

      if (props.mode === 'edit') {
       await editActivity({ data: newObj, activityTypeID: activityType, activityID: props.id });
      } else {
        await addActivity(newObj, activityType);
      }

      reset();
      closePopup();
    } catch (error) {
      console.error('Error in onSubmit:', error);
    }
  };

  const inputsDetails = [
    {
      id: 1,

      name: 'title',
      label: 'نام فعالیت',
      iconName: 'popUpNote',
      type: 'text',
      errors: errors['title'],
      value: watch('title'),
      rules: { required: 'این فیلد الزامی است' },
      defaultValue: props.defaultValues?.title || '',
    },
    {
      id: 2,
      name: 'description',
      label: 'توضیحات',
      iconName: 'copy',
      type: 'textarea',
      errors: errors['description'],
      value: watch('description'),
      defaultValue: props.defaultValues?.description || '',
    },
    {
      id: 3,
      name: 'date',
      label: 'تاریخ',
      iconName: 'calendar',
      onChange: handleDateChange,
      time: endTime,
      defaultValue: props.defaultValues?.date || '',
    },
    {
      id: 4,
      name: 'startTime',
      label: 'از ساعت',
      iconName: 'clock',
      time: startTime,
      rules: { required: 'این فیلد الزامی است' },
      onChange: handleStartTimeChange,
      defaultValue: props.defaultValues?.startTime || '',
    },
    {
      id: 5,
      name: 'endTime',
      label: 'تا ساعت',
      iconName: 'clock',
      time: endTime,
      onChange: handleEndTimeChange,
      defaultValue: props.defaultValues?.endTime || '',
    },
    {
      id: 6,
      name: 'activityType',
      label: 'نوع فعالیت',
      iconName: 'popUpWeight',
      type: 'select',
      errors: errors['activityType'],
      value: watch('activityType'),
      rules: { required: 'این فیلد الزامی است' },
      dropDownList: activityTypesList || [],
      defaultValue: props.defaultValues?.activityType || '',
    },
  ];

  const imageUploader = [
    {
      id: 1,
      iconName: 'plus',
    },
    {
      id: 2,
      iconName: 'plus',
    },
    {
      id: 3,
      iconName: 'plus',
    },
  ];
  return (
    
    <form onSubmit={handleSubmit(onSubmit)}>
        <div className={styles['popup']}>
         <div className={styles['popup__header']}>
         <ArrowForwardIcon  onClick={closePopup} className={styles['header__icon']} />
         <span className={styles['header__title']}>
          ایجاد فعالیت
          </span>
         </div>
         
         <div className={styles['popup__photos']}>
          <span className={styles['photos__title']}>عکس ها</span>
       <div className={styles['photos__content']}>
       {
        imageUploader.map((item) => (
     
          <CustomImageUploader key={item.id} {...item}   image={image[item.id-1]} onChange={(value) => handleImageChange(item.id, value)} />
        ))
        }
       </div>
  
            
         </div>
    <div className={styles['popup__inputs']}>
    {
  inputsDetails.map((input) => (
    input.type === "textarea" && (
      <CustomTextarea 
        inputdetails={{...register(input.name, input.rules) }}
        {...input}
        key={input.id}
      />
    )
    ||
    (input.name === "date" && (
      <CustomDatePicker {...input} key={input.id} />
    ))
    ||
    (input.type === "select" && (
      <CustomSelect
        inputdetails={{...register(input.name, input.rules) }}
        {...input}
        key={input.id}
      />
    ))
    ||
    ((input.name === "startTime" || input.name === "endTime") && (
      <div className={styles['popup--half']} key={input.id}>
        <CustomTimePicker {...input}></CustomTimePicker>
      </div>
    ))
    ||
    (
      <CustomInput
        inputdetails={{...register(input.name, input.rules) }}
        {...input}
        key={input.id}
      />
    )
  ))
  
   
  }
   
    </div>
    <div className={styles['popup__btns']}>
    
  
    <div className={styles['popup--half']}>
      <CustomButton type='button' color='secondary' text={"لغو "} onClick = {closePopup}></CustomButton>
      
    </div>
    <div className={styles['popup--half']}>
      <CustomButton text={"تایید"}></CustomButton>
      
    </div>
    </div>
    
       
  
      </div>
    </form>
    )

}