import React from 'react'
import styles from "./ShowActivityType.module.scss"
import CustomButton from '@components/Common/customButton/CustomButton'

export default function ShowActivityType({closePopup =()=>{},...props}) {
  return (
    <div className={styles['pop-up']}>
        <span className={styles['pop-up__title']}>
        مشاهده نوع فعالیت
        </span>
  <p className={styles['activity-type-name']}> {props.activityType}</p>
  <div className={styles['popup-btns']}>
    <CustomButton text={"تایید"}  onClick = {closePopup}></CustomButton>
  </div>
    </div>
  )
}
