import React, { useEffect } from "react";
import { useState } from "react";
import styles from "./User.module.scss";
import UserInfo from "@components/userInfo/UserInfo";
import "react-calendar/dist/Calendar.css";
import "./custom.scss";
import "@hassanmojab/react-modern-calendar-datepicker/lib/DatePicker.css";
import { Calendar } from "@hassanmojab/react-modern-calendar-datepicker";
import { useEventsContext } from "@/context/EventsContext";
import EventItem from "../Common/eventItem/EventItem";

export default function User() {
    var moment = require("jalali-moment");

    const { events, getEvents } = useEventsContext();
    useEffect(() => {
        getEvents();
    }, []);
    const toDay = new Date().setHours(12, 0, 0, 0);

    const [selectedDay, setSelectedDay] = useState(null);

    const persianDate = moment
        .from(
            `${selectedDay?.year}/${selectedDay?.month}/${selectedDay?.day}`,
            "fa",
            "YYYY/MM/DD"
        )
        .format("YYYY/MM/DD");
    let date = new Date(persianDate).setHours(12, 0, 0, 0) || toDay;

    const filteredEvents = events.filter(
        (event) => new Date(event.date).setHours(12, 0, 0, 0) == date
    );

    return (
        <div className={styles["user-section"]}>
            <UserInfo />

            <Calendar
                value={selectedDay}
                onChange={setSelectedDay}
                shouldHighlightWeekends
                locale="fa"
            />

            <div className={styles["events"]}>
                {filteredEvents ? (
                    filteredEvents.map((event) => (
                        <EventItem {...event}></EventItem>
                    ))
                ) : (
                    <EventItem></EventItem>
                )}
            </div>
        </div>
    );
}
