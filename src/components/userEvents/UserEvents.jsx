import React from 'react'
import styles from "./UserEvents.module.scss"
import {UserEventItem} from '@components/Common'
export default function Events() {
  return (
    <div className={styles["events"]}>
    <div className={styles["events-details"]}>
      <p className={styles["events-title"]}> رویداد ها</p>
    </div>
    <div className={styles["event-items"]}>
     <UserEventItem></UserEventItem>
    </div>
  </div>
  )
}
