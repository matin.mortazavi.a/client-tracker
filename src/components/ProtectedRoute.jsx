import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import localStorageService from "@/services/localStorageService"
const ProtectedRoute = () => {
 const isLoggedin = localStorageService.get("userToken")
 
  return isLoggedin ? <Outlet/> : <Navigate to="/login" />;
};

export default ProtectedRoute;




