import React, { useState } from 'react'
import styles from "./ActivityTypeMethodes.module.scss"
import DeleteActivityType from '@components/deleteActivityType/DeleteActivityType'
import EditActivityType from '@components/editActivityType/EditActivityType'
import ShowActivityType from '@components/showActivityType/ShowActivityType'
import CustomButton from "@components/Common/customButton/CustomButton"
export default function ActivityTypeMethodes({showActivityTypeMethodes=()=>{},...props}) {
    const [showDeletePopup,setShowDeletePopup] = useState(false)
    const [showEditPopup,setShowEditPopup] = useState(false)
    const [showShowPopup,setShowShowPopup] = useState(false)

   
    const closePopups=()=>{
        setShowDeletePopup(false)
        setShowEditPopup(false)
        setShowShowPopup(false)
    }
  return (
    <>
    <div className={styles['backdrop']}>
    </div>
    <div className={styles['pop-up']}>
    {showDeletePopup && (
        <DeleteActivityType closePopup = {closePopups} id = {props.activityTypeDetails.id}></DeleteActivityType>
    )}
    {showEditPopup && (
        <EditActivityType closePopup = {closePopups} defaultValue = {props.activityTypeDetails.activityType} id = {props.activityTypeDetails.id}></EditActivityType>
    )}
    {showShowPopup && (
        <ShowActivityType closePopup = {closePopups} activityType = {props.activityTypeDetails.activityType}></ShowActivityType>
    )}
    <CustomButton onClick={()=>setShowEditPopup(true)} text="ویرایش"></CustomButton>
    <CustomButton onClick={()=>setShowDeletePopup(true)} text="حدف"></CustomButton>
    <CustomButton onClick={()=>setShowShowPopup(true)} text="مشاهده"></CustomButton>
    <CustomButton onClick={()=>showActivityTypeMethodes(false)} text="بستن"></CustomButton>
    </div></>
  )
}
