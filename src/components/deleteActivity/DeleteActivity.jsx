import React from 'react'
import styles from "./DeleteActivity.module.scss"
import { useActivitesContext } from '@/context/ActivitesContext'
import CustomButton from "@components/Common/customButton/CustomButton"
import {Alert} from "@components/Common"
import { useState } from 'react'
export default function DeleteActivity({closePopup=()=>{},...props}) {
    const [showAlert, setShowAlert] = useState(false);
    const {deleteActivity} = useActivitesContext()
    const onClickHandler = async()=>{
       
        try{
            await deleteActivity(props.activityType,props.activityID)
            closePopup()
        }catch(error){
            setShowAlert(true);
            setTimeout(() => {
              setShowAlert(false);
            }, 2000);
        }
       
    }
  return (

    <>
    <div className={styles['backdrop']}>
        
    </div>
    <div className={styles['pop-up']}>
    {showAlert && (
        <Alert text="طلاعات وارد شده اشتباه است" title="خطا"></Alert>
      )}

    <CustomButton text={"تایید"}  onClick={onClickHandler}></CustomButton>
    <CustomButton type='button' color='secondary' text={"لغو "} onClick = {closePopup}></CustomButton>
        
        
    </div></>
  )
}
