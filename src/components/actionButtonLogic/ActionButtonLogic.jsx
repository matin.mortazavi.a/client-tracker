import React, { useState,lazy,Suspense } from 'react'

import {FloatingActionButton} from '@components/Common'
const EventForm = lazy(() => import('@components/eventForm/EventForm'));
const ActivityForm = lazy(() => import('@components/activityForm/ActivityForm'));
const NewActivityType = lazy(() => import('@components/newActivityType/NewActivityType'))

export default function ActionButtonLogic() {
const [showActivityPopup ,setShowActivityPopup  ] = useState(false)
const [showActivityTypePopup ,setShowActivityTypePopup] = useState(false)
const [showEventPopup ,setShowEventPopup] = useState(false)


 const closePopup = ()=>{
  setShowActivityPopup(false)
  setShowActivityTypePopup(false)
  setShowEventPopup(false)
 }


  return (
   
    <div>
      {showActivityPopup && (
       <Suspense fallback = {<div>loading...</div>}>
         <ActivityForm closePopup = {closePopup}/>
       </Suspense>
         )}
      {showActivityTypePopup && 
      <Suspense fallback = {<div>loading...</div>}>
        <NewActivityType closePopup = {closePopup} />
      </Suspense>
     }
     {showEventPopup && 
      <Suspense fallback = {<div>loading...</div>}>
        <EventForm closePopup = {closePopup} />
      </Suspense>
     }
      
       
      <FloatingActionButton
          setShowActivityTypePopup = {setShowActivityTypePopup}
          setShowActivityPopup = {setShowActivityPopup} 
          setShowEventPopup = {setShowEventPopup}
          />
      </div>
  )
}
