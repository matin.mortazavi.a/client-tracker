import React from 'react'
import styles from "./formFieldLayout.module.scss"
import SvgLoader from '@components/SvgLoader'


export default function FormFieldLayout(props) {


  return (
   
 <>
    <div 
    className={ 

       props.isNotEmpty
        ? `${styles.notEmpty} ${styles.formField}`
        : `${styles.formField}`
    }
  >
   
          
          <SvgLoader  className={styles["form__icon"]}
               name={props.iconName}
               onClick ={props.handlePasswordVisibility}
             
              />
         
          <label>{props.label}</label>
        {props.children}
   
  
          <SvgLoader  className={styles["password-icon"]}
          name={props.passIconName}
         onClick={props.handlePasswordVisibility}/>

              
  </div>
   {props.errors && (
        <p className={styles["error-messege"]}>{props.errors.message}</p>
      )}
  </>
  )
}
