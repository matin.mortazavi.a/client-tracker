import React from 'react'
import styles from "./AuthLayout.module.scss"
import img from "@/assets/images/img.png";
export default function AuthLayout({name,children}) {
 
    return (
        <div className={styles["form-wrapper"]}>
         
          <div className={styles["form-box"]}>
            <div className={styles.form}>
              <img className={styles["form__img"]} src={img} alt="not founded" />
              <h2 className={styles["form__title"]}>{name} </h2>
            {children}
            </div>
          </div>
        </div>
      );
}
