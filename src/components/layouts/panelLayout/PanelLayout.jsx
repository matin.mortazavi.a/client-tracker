import React from 'react'
import SideBar from '@components/Common/sidebar/Sidebar.js'
import ActionButtonLogic from '@components/actionButtonLogic/ActionButtonLogic'
import User from "@components/user/User.js"
import styles from "./PanelLayout.module.scss"

export default function PanelLayout({children}) {
  return (
    <div className= {`${styles.panel} ${styles.container}`} >
        <div className={styles["sidebar"]}>
          <SideBar />
          
        </div>
        <ActionButtonLogic></ActionButtonLogic>
       <div className={styles["main-section"]}>
        {children}
        </div> 
        <div className={styles["user"]}>
          <User></User>
        </div>
       </div>

  )
}
