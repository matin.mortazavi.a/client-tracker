const {
  override,
  addWebpackAlias,
  adjustStyleLoaders,
} = require("customize-cra");
const path = require("path");

module.exports = override(

  addWebpackAlias({
    "@components": path.resolve(__dirname, "src/components"),
    "@": path.resolve(__dirname, "src"),
  }),

  adjustStyleLoaders((rule) => {
    if (rule.test.toString() === /\.scss$/.toString()) {
      rule.use.push({
        loader: "style-resources-loader",
        options: {
          patterns: path.resolve(__dirname, "src/styles/base/_reset.scss"),

        },
      });
    }
  })
);
